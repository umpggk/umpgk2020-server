# Serwer zawodów UMPGGK 2020

Uczelniane Mistrzostwa Programów Grających w gry Kombinatoryczne są cyklicznym konkursem rozgrywanym w Instytucie Informatyki Uniwersytetu Śląskiego. 
Niniejsze repozytorium zawiera kod serwera służącego do rozegrania gier turniejowych w grze __King's Valley__ w VI edycji UMPGGK.
Niniejszy opis zawiera skrócone zasady gry w __King's Valley__, aczkolwiek zalecamy zapoznanie się z pełnymi zasadami 
na [tej](http://www.logygames.com/english/kingsvalley.html) stronie.

## Instalacja serwera

Do instalacji serwera oraz jego uruchomienia potrzebne są narzędzia [git](https://git-scm.com/) oraz [node.js](https://nodejs.org/en/).
W czasie testów korzystaliśmy z __node.js__ w wersji 11.1.0 oraz __git__ w wersji 2.24.1 (aczkolwiek najnowsze wersje powinny działać bezproblemowo).

Aby zainstalować serwer należy pobrać pliki z repozytorium oraz ściągnąć wymagane biblioteki przy pomocy narzędzia **npm** (które powinno być zainstalowane wraz z __node.js__):
```
git clone https://gitlab.com/ii-us/umpggk/umpgk2020-server
cd umpggk2020-server
npm install
```
Serwer jest gotowy do uruchomienia.

## Uruchomienie serwera

```
Komenda:
    npm start -- [parametry]
Parametry (wszystkie parametry są opcjonalne)
    --port={port}
        port serwera, domyślnie 6789
    --register={nazwa pliku}
        plik, w którym zapisane zostaną dane turnieju, domyślnie: ./tournament-register.json
    --system={system}
        system parowania graczy, dostępne opcje: roundrobin (domyślnie) oraz mcmahon
    --nofgames={liczba gier}
        liczba gier w pojedynczym meczu, domyślnie 10
    --timelimit={limit w ms}
        limit czasowy na pojedynczy ruch, domyślnie 2000
    --autostart
        uruchamia tryb automatycznego startu kolejnych rund, po za pierwszą rundą
    --nosaving
        flaga wyłącza zapis turnieju do pliku
    --restore
        przywróć dane turnieju z pliku
```

Przykład:
```
npm start -- --autostart --nofgames=1
```

Uruchomiony serwer nasłuchuje na uczestników turnieju, aby rozpocząć pierwszą rundę należy wprowadzić w terminalu, w którym uruchomiono serwer, komendę administratora `start`.

Przykład:
```
03/03/2019 14:06:47.962 Info: The server is listening on the address: :: and the port: 6789
start
03/03/2019 14:08:08.061 Info: The ADMINISTRATOR's command received: start
03/03/2019 14:08:08.065 Info: The round #0 has been started with 0 matches
03/03/2019 14:08:08.066 Info: No matches were found hence the tournament is over
```

## Turniej

Turniej składa się z rund, których liczba wyliczana jest w zależności od liczby zawodników i systemu. Rundy
składają się z meczy, mecze składają się z gier. Wynik meczu określany jest na podstawie stosunku wygranych gier pomiędzy zawodnikami (możliwy remis).

Turniej rozgrywany jest automatycznie, kolejne rundy uruchamiane są ręcznie (za pomocą komendy `start`) lub samoczynnie (uruchomienie serwera z parameterem `--autostart`).

## Skrócone zasady gry _King's Valley_

- Gra odbywa się na planszy 5x5 z oznaczonym polem centralnym, tj. tytułowym _King's Valley_.
- Każdy z graczy, tj. gracz biały i czarny, ma do dyspozycji króla oraz 4 piony.
- Gracze rozpoczynają grę w tzw. pozycji _Retrieve the King_ (zobacz poniżej).
- Zadaniem graczy jest umieszczenie swojego króla w polu centralnym.
- Gracze wykonują naprzemienne ruchy swoimi figurami, gracz biały rozpoczyna grę.
- Gracz musi ruszyć pionem lub królem w swojej turze.
- Wszystkie figury poruszają się w jednakowy sposób, tzn. wykonują ruch podobny do ruchu szachowego hetmana z tym, że nie można zbijać innych figur, jak również ruchu nie można
zakończyć na dowolnym polu. Ruch figury musi zakończyć się na bandzie planszy albo przed innym pionem lub królem.
- Piony nie mogą zatrzymać się na polu centralnym.
- Pierwszy ruch w grze musi być wykonany pionem.
- Gracz przegrywa jeżeli po jego turze, jego król nie może wykonać żadnego ruchu (zobacz poniżej).

```
+---------------+
| w  w  B  w  w |
| .  .  .  .  . |
| .  .  .  .  . |
| .  .  .  .  . |
| b  b  W  b  b |
+---------------+
```
Pozycja początkowa (przez `w` oznaczone zostały piony białego, a `W` do biały król, analogicznie `b` to czarne piony, a `B` to ich król).

```
+---------------+
| .  w  B  w  . |
| .  W  w  w  . |
| .  .  .  .  . |
| .  .  .  .  . |
| b  b  .  b  b |
+---------------+
```
Zablokowany czarny król (czarny nie ma ruchu, który uwolni króla).

```
+---------------+
| w  w  .  w  w |
| .  .  b  .  . |
| .  .  W  .  B |
| .  .  .  .  . |
| b  b  .  .  b |
+---------------+
```
Biały wygrywa grę (zajęcie pola centralnego).

## Protokół komunikacyjny

Protokół komunikacyjny oparty jest o standardowe gniazdka sieciowe, każda komenda wysyłana z lub do serwera powinna kończyć się znakiem nowej linii. 
Używane w protokole koordynaty pozycji są zgodne z opisem poniższej planszy:
```
    0  1  2  3  4
  +---------------+
0 | w  w  B  w  w |
1 | .  .  .  .  . |
2 | .  .  .  .  . |
3 | .  .  .  .  . |
4 | b  b  W  b  b |
  +---------------+
```
Powyższa plansza przedstawia startową pozycję (x,y) pionów dla gracza białego i czarnego, tj.
- biały król jest na pozycji (2,4) a białe piony na pozycjach: (0,0), (1,0), (3,0) oraz (4,0);
- czarny król jest na pozycji (2,0) a czarne piony na pozycjach: (0,4), (1,4), (3,4) oraz (4,4);

### Żądania klient (wysyłają programy grające w turnieju)

```
100 [nazwa_gracza] // Podłącz się jako gracz, nazwa gracza nie może zawierać białych znakow
210 [pozycja]      // Wyślij ruch, gdzie pozycja to wspołrzędne ruchu "skąd dokąd" (jedna spacja w środku, koordynaty bez spacji z przecinkiem)
```

Przykłady:
```
100 testowy_gracz
210 0,0 3,3
```

### Odpowiedzi serwera

```
200 [opis gry] // Komunikat oznacza rozpoczęcie nowej gry, należy oczekiwać koloru gracza rozmiarow planszy oraz pozycji początkowej
220 [pozycja]  // Nowa pozycja przeciwnika, serwer oczekuje na Twój ruch, zgodnie z pozycją wyzej
230            // Wygrałeś wg. zasad
231            // Wygrałeś przez przekroczenie czasu (przeciwnika)
232            // Wygrałeś przez rozłączenie się przeciwnika
240            // Przegrałeś wg. zasad
241            // Przegrałeś przez przekroczenie czasu
250            // Remis
299 [miejsce]  // Koniec turnieju
999 [opis]     // Błąd komendy, opis powinien wyjaśnić przyczynę
```

Przykłady:
```
200 white
220 3,3 0,0
299 3
```

## Losowi gracze

Serwer posiada implementację losowych graczy (można stworzyć ich dowolną liczbę), aby ich uruchomić należy posłużyć się następującą komendą:
```
Komenda:
    node ./src/client/index.js [parametry]
Parametry (wszystkie parametry są opcjonalne)
    --host={host}
        adres IP serwera, domyślnie 127.0.0.1
    --port={port}
        port serwera, domyślnie 6789
    --nofclients={liczba klientów}
        liczba losowych klientów, domyślnie 2
```

## Uwaga

Serwer jest ukończony aczkolwiek autor zastrzega sobie prawo do jego modyfikacji.
