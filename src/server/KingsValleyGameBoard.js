const log = require('./log.js')(__filename);
/**
 * Errors codes:
 * 1 -> king on the king's valley tile // winner
 * 2 -> the invalid move's coordinates // loser
 * 4 -> my king is blocked // loser
 * 5 -> draw, position was repeated 3 times // doesnt matter
 * 6 -> no moves // loser
 * Exception format:
 * [code, player]
 */

/**
 * (0,0)
 *     +-----+
 *    0|wwBww|
 *    1|.....|
 *    2|.....|
 *    3|.....|
 *    4|bbWbb|
 *     +-----+
 *      01234
 */

module.exports = class KingsValleyGameBoard
{
	constructor() {
		this.reset();
	}
	reset() {
		this.boardSize = 5;
		this.nofPieces = 5;
        this.kingsValleyTile = '2,2';
        this.positions = {
            // 0-index is a king
            white: ['2,4', '0,0', '1,0', '3,0', '4,0'],
            black: ['2,0', '0,4', '1,4', '3,4', '4,4']
        };
        this.player = 'white';
        this.firstMove = true;
        this.history = [this.toString()];
	}
    toString() {
        return 'white:' + this.positions.white.join(';') + '|black:' + this.positions.black.join(';');
    }
    toPrintableBoard() {
		let board = '  ';
		for (let x = 0; x < this.boardSize; ++x) {
			board += '| ' + x + ' ';
		}
		for (let y = 0; y < this.boardSize; ++y) {
			board += '\n' + ' ' + y;
			for (let x = 0; x < this.boardSize; ++x) {
				let p = '.';
                if (this.positions.white.includes(x + ',' + y)) {
                    p = this.positions.white.indexOf(x + ',' + y) === 0 ? 'W' : 'w';
                } if (this.positions.black.includes(x + ',' + y)) {
                    p = this.positions.black.indexOf(x + ',' + y) === 0 ? 'B' : 'b';
                }
				board += '| ' + p + ' ';
            }
		}
        let availableMoves = this.getAvailableMoves();
        board += "\n white moves: " + availableMoves.white.join('; ');
        board += "\n black moves: " + availableMoves.black.join('; ');
		return board;
	}
    getAvailableMoves() {
        let availableMoves = {
            white: [],
            black: [],
            nofKingMoves: {
                white: 0,
                black: 0
            }
        };
        for (let player of ['white', 'black']) {
            for (let i = 0; i < this.nofPieces; ++i) {
                if (this.firstMove && i === 0) {
                    continue;
                }
                let from = this.positions[player][i].split(',').map(n => parseInt(n));
                for (let offset of [[1,0],[-1,0],[0,1],[0,-1],[1,1],[-1,1],[1,-1],[-1,-1]]) {
                    let to = [from[0], from[1]];
                    let x = from[0] + offset[0];
                    let y = from[1] + offset[1];
                    while (
                        x < this.boardSize && x >= 0
                        && y < this.boardSize && y >= 0
                        && this.positions['white'].includes(x + ',' + y) === false
                        && this.positions['black'].includes(x + ',' + y) === false
                    ) {
                        to = [x, y];
                        x = to[0] + offset[0];
                        y = to[1] + offset[1];
                    }
                    to = to[0] + ',' + to[1];
                    if (to !== this.positions[player][i] && (to !== this.kingsValleyTile || i === 0)) {
                        if (i === 0) {
                            availableMoves.nofKingMoves[player]++;
                        }
                        availableMoves[player].push(this.positions[player][i] + '->' + to);
                    }
                }
            }
        }
        return availableMoves;
    }
    makeMove(from, to) {
        let availableMoves = this.getAvailableMoves();
        if (availableMoves[this.player].includes(from + '->' + to) === false) {
            throw [2, this.player];
        }
        this.positions[this.player][this.positions[this.player].indexOf(from)] = to;
        if (to === this.kingsValleyTile) {
            throw [1, this.player];
        }
        if (this.firstMove === false && this.getAvailableMoves().nofKingMoves[this.player] === 0) {
            throw [4, this.player];
        }
        this.player = this.player === 'black' ? 'white' : 'black';
        if (this.getAvailableMoves()[this.player].length === 0) {
            throw [6, this.player];
        }
        this.history.push(this.toString());
        if (this.history.reduce((counter, board) => counter + (board === this.toString() ? 1 : 0), 0) >= 3) {
            throw [5, this.player];
        }
        this.firstMove = false;
    }
}