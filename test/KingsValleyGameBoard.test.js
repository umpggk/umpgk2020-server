const KingsValleyGameBoard = require('./../src/server/KingsValleyGameBoard.js');

test('after initialization the board should be on the initial layout', () => {
	let board = new KingsValleyGameBoard();
	expect(board.toString()).toBe('white:2,4;0,0;1,0;3,0;4,0|black:2,0;0,4;1,4;3,4;4,4');
});

test('all available moves should be generated', () => {
	let board = new KingsValleyGameBoard();
    let availableMoves = board.getAvailableMoves();
	expect(availableMoves.white.length).toBe(10);
	expect(availableMoves.black.length).toBe(10);
    // order of moves generating: [1,0],[-1,0],[0,1],[0,-1],[1,1],[-1,1],[1,-1],[-1,-1]
    expect(availableMoves.white).toEqual([
        '0,0->0,3', '0,0->3,3',
        '1,0->1,3', '1,0->4,3', '1,0->0,1',
        '3,0->3,3', '3,0->4,1', '3,0->0,3',
        '4,0->4,3', '4,0->1,3'
    ]);
    board.firstMove = false;
    availableMoves = board.getAvailableMoves();
	expect(availableMoves.white.length).toBe(13);
	expect(availableMoves.black.length).toBe(13);
});

test('the board should be altered after making a move', () => {
	let board = new KingsValleyGameBoard();
	board.makeMove('0,0', '3,3');
	expect(board.toString()).toBe('white:2,4;3,3;1,0;3,0;4,0|black:2,0;0,4;1,4;3,4;4,4');
});

test('the invalid move exception should be thrown', () => {
	let board = new KingsValleyGameBoard();
	expect(board.makeMove.bind(board, '0,0', '4,3')).toThrow('2,white');
});

test('three times repeated position exception should be thrown', () => {
	let board = new KingsValleyGameBoard();
    board.makeMove('0,0', '0,3');
    board.makeMove('4,4', '4,1');
    board.makeMove('0,3', '0,0');
    board.makeMove('4,1', '4,4');
    board.makeMove('0,0', '0,3');
    board.makeMove('4,4', '4,1');
    board.makeMove('0,3', '0,0');
	expect(board.makeMove.bind(board, '4,1', '4,4')).toThrow('5,white');
});

test('blocked king exception should be thrown', () => {
	let board = new KingsValleyGameBoard();
    board.firstMove = false;
    board.positions.white = ['2,4', '2,3', '3,3', '3,0', '4,0'];
    board.positions.black = ['2,0', '1,3', '1,4', '3,4', '4,4'];
	expect(board.makeMove.bind(board, '4,0', '4,3')).toThrow('4,white');
});

test('king on the kings valley tile exception should be thrown', () => {
	let board = new KingsValleyGameBoard();
    board.firstMove = false;
    board.positions.white = ['2,4', '0,0', '1,0', '3,0', '4,0'];
    board.positions.black = ['2,1', '0,4', '1,4', '3,4', '4,4'];
	expect(board.makeMove.bind(board, '2,4', '2,2')).toThrow('1,white');
});

test('no moves exception should be thrown', () => {
	let board = new KingsValleyGameBoard();
    board.positions.white = ['2,0', '0,0', '1,0', '3,0', '4,0'];
    board.positions.black = ['2,4', '0,1', '1,1', '3,1', '4,1'];
    board.firstMove = false;
    board.player = 'black';
	expect(board.makeMove.bind(board, '2,4', '2,1')).toThrow('6,white');
});
